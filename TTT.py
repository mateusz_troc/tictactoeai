class TTT:
    from random import choice
    def __init__(self):
        self.cross, self.circel = 'X', 'O'
        self.size = 3
        self.board = [[' ' for _ in range(self.size)] for _ in range(self.size)]
    def displayBoard(self):
        for i in range(self.size - 1, -1, -1):
            for _ in range(self.size):
                print("+-------", end="")
            else:
                print("+")
            for _ in range(self.size):
                print("|       ", end="")
            else:
                print("|")
            print("|   ", end="")
            for x in range(self.size - 1):
                print(self.board[i][x], end="")
                print("   |   ", end="")
            else:
                print(self.board[i][self.size - 1], end="")
                print("   |")
            for _ in range(self.size):
                print("|       ", end="")
            else:
                print("|")
        else:
            for _ in range(self.size):
                print("+-------", end="")
            else:
                print("+")
    def isMovesLeft(self):
        for i in range(self.size):
            for j in range(self.size):
                if self.board[i][j] == ' ':
                    return True
        return False
    def evaluate(self, player, enemy):
        for i in range(self.size):
            x = self.size
            for j in range(self.size):
                if self.board[i][j] == player:
                    x -= 1
            if x == 0:
                return 10
        for i in range(self.size):
            x = self.size
            for j in range(self.size):
                if self.board[i][j] == enemy:
                    x -= 1
            if x == 0:
                return -10

        for i in range(self.size):
            x = self.size
            for j in range(self.size):
                if self.board[j][i] == player:
                    x -= 1
            if x == 0:
                return 10
        for i in range(self.size):
            x = self.size
            for j in range(self.size):
                if self.board[j][i] == enemy:
                    x -= 1
            if x == 0:
                return -10

        x = self.size
        for i in range(self.size):
            if self.board[i][i] == player:
                x -= 1
        if x == 0:
            return 10
        x = self.size
        for i in range(self.size):
            if self.board[i][i] == enemy:
                x -= 1
        if x == 0:
            return -10

        x = self.size
        for i in range(self.size):
            if self.board[i][(self.size - 1) - i] == player:
                x -= 1
        if x == 0:
            return 10
        x = self.size
        for i in range(self.size):
            if self.board[i][(self.size - 1) - i] == enemy:
                x -= 1
        if x == 0:
            return -10

        return 0
    def minimax(self, depth, isMax, player, enemy):
        score = self.evaluate(player, enemy)
        if score == 10:
            return score
        if score == -10:
            return score
        if self.isMovesLeft() is False:
            return 0

        if isMax:
            best = -1000
            for i in range(self.size):
                for j in range(self.size):
                    if self.board[i][j] == ' ':
                        self.board[i][j] = player
                        best = max(best, self.minimax(depth + 1, not isMax, player, enemy))
                        self.board[i][j] = ' '
            return best
        else:
            best = 1000
            for i in range(self.size):
                for j in range(self.size):
                    if self.board[i][j] == ' ':
                        self.board[i][j] = enemy
                        best = min(best, self.minimax(depth + 1, not isMax, player, enemy))
                        self.board[i][j] = ' '
            return best
    def findBestMove(self, player, enemy):
        bestVal = -1000
        bestMove = (-1, -1)
        for i in range(self.size):
            for j in range(self.size):
                if self.board[i][j] == ' ':
                    self.board[i][j] = player
                    moveVal = self.minimax(0, False, player, enemy)
                    self.board[i][j] = ' '
                    if moveVal > bestVal:
                        bestMove = (i, j)
                        bestVal = moveVal
        self.board[bestMove[0]][bestMove[1]] = player
        return bestMove
    def player(self, player):
        while True:
            i = int(input("Player move({}): ".format(player)))
            i = ((i - 1) // self.size, (i - 1) % self.size)
            if self.board[i[0]][i[1]] == ' ':
                break
        self.board[i[0]][i[1]] = player
    def gamecvc(self):
        while self.isMovesLeft() is True and self.evaluate(self.cross, self.circel) == 0:
            self.findBestMove(self.circel, self.cross)
            self.displayBoard()
            if self.isMovesLeft() is False or self.evaluate(self.cross, self.circel) != 0:
                break
            self.findBestMove(self.cross, self.circel)
            self.displayBoard()
        if self.evaluate(self.cross, self.circel) == 10:
            print("X won")
        elif self.evaluate(self.cross, self.circel) == -10:
            print("0 won")
        elif self.evaluate(self.cross, self.circel) == 0:
            print("Tie")
    def gamepvc(self):
        i = self.choice([1, 2])
        if i == 1:
            self.findBestMove(self.cross, self.circel)
        while self.isMovesLeft() is True and self.evaluate(self.cross, self.circel) == 0:
            self.displayBoard()
            self.player(self.circel)
            if self.isMovesLeft() is False or self.evaluate(self.cross, self.circel) != 0:
                break
            self.findBestMove(self.cross, self.circel)
        self.displayBoard()
        if self.evaluate(self.cross, self.circel) == 10:
            print("X won")
        elif self.evaluate(self.cross, self.circel) == -10:
            print("0 won")
        elif self.evaluate(self.cross, self.circel) == 0:
            print("Tie")
    def gamepvp(self):
        i = self.choice([1, 2])
        if i == 1:
            self.displayBoard()
            self.player(self.cross)
        while self.isMovesLeft() is True and self.evaluate(self.cross, self.circel) == 0:
            self.displayBoard()
            self.player(self.circel)
            if self.isMovesLeft() is False or self.evaluate(self.cross, self.circel) != 0:
                break
            self.displayBoard()
            self.player(self.cross)
        self.displayBoard()
        if self.evaluate(self.cross, self.circel) == 10:
            print("X won")
        elif self.evaluate(self.cross, self.circel) == -10:
            print("0 won")
        elif self.evaluate(self.cross, self.circel) == 0:
            print("Tie")
    def tableSize(self, size):
        if 0 < size:
            self.size = size
    def randomboard(self):
        self.board = [[self.choice([' ', 'X','O']) for _ in range(self.size)] for _ in range(self.size)]
x = TTT()
x.tableSize(2)
x.randomboard()
x.gamepvc()

